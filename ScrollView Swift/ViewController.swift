//
//  ViewController.swift
//  ScrollView Swift
//
//  Created by Francisco Morales on 9/1/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let scrollview = UIScrollView(frame:self.view.bounds)
        scrollview.backgroundColor = UIColor.clear
        self.view.addSubview(scrollview)
        scrollview.isPagingEnabled = true
        scrollview.contentSize = CGSize(width: self.view.frame.size.width * 3, height: self.view.frame.size.height * 3)
        
        //ROW 1
        let redView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        redView.backgroundColor = UIColor.red
        scrollview.addSubview(redView)
        
        let greenView = UIView(frame: CGRect(x: self.view.frame.size.width, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        greenView.backgroundColor = UIColor.green
        scrollview.addSubview(greenView)
        
        let yellowView = UIView(frame: CGRect(x: self.view.frame.size.width * 2, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        yellowView.backgroundColor = UIColor.yellow
        scrollview.addSubview(yellowView)
        
        //ROW 2
        let orangeView = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        orangeView.backgroundColor = UIColor.orange
        scrollview.addSubview(orangeView)
        
        let purpleView = UIView(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        purpleView.backgroundColor = UIColor.purple
        scrollview.addSubview(purpleView)
        
        let grayView = UIView(frame: CGRect(x: self.view.frame.size.width * 2, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        grayView.backgroundColor = UIColor.gray
        scrollview.addSubview(grayView)
        
        //ROW 3
        let blackView = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height * 2, width: self.view.frame.size.width, height: self.view.frame.size.height))
        blackView.backgroundColor = UIColor.black
        scrollview.addSubview(blackView)
        
        let cyanView = UIView(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height * 2, width: self.view.frame.size.width, height: self.view.frame.size.height))
        cyanView.backgroundColor = UIColor.cyan
        scrollview.addSubview(cyanView)
        
        let blueView = UIView(frame: CGRect(x: self.view.frame.size.width * 2, y: self.view.frame.size.height * 2, width: self.view.frame.size.width, height: self.view.frame.size.height))
        blueView.backgroundColor = UIColor.blue
        scrollview.addSubview(blueView)
        
        
        
        
        
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

